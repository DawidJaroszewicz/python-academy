# Makefile
SRC_DIR = forum
SHELL := /bin/bash
PROJECT = PythonAcademyForum
VENV_DIR ?= venv

ifeq ($(OS),Windows_NT)
	PY_VERSION = python
else
	PY_VERSION = python3
endif

DEVELOPMENT_MODE=true
DATABASE_URL?=postgres://postgres:postgres@localhost:5432/forumdb

# DJANGO

createsuperuser:
	cd $(SRC_DIR) && \
	DEVELOPMENT_MODE=$(DEVELOPMENT_MODE) \
	DATABASE_URL=$(DATABASE_URL) \
	$(PY_VERSION) manage.py createsuperuser --username=ryro --email=dawidjaroszewicz@wp.pl

makemessages:
	cd $(SRC_DIR) && $(PY_VERSION) manage.py makemessages -l pl

compilemessages:
	cd $(SRC_DIR) && $(PY_VERSION) manage.py compilemessages -l pl

migrate:
	cd $(SRC_DIR) && \
	DEVELOPMENT_MODE=$(DEVELOPMENT_MODE) \
	DATABASE_URL=$(DATABASE_URL) \
	$(PY_VERSION) manage.py migrate

makemigrations:
	cd $(SRC_DIR) && \
	DEVELOPMENT_MODE=$(DEVELOPMENT_MODE) \
	DATABASE_URL=$(DATABASE_URL) \
	$(PY_VERSION) manage.py makemigrations

tests:
	chmod +x .cicd/scripts/run-tests.sh && \
	cd $(SRC_DIR) && \
	DEVELOPMENT_MODE=$(DEVELOPMENT_MODE) \
	DATABASE_URL=$(DATABASE_URL) \
	PYTHONPATH=$(SRC_DIR) \
	../.cicd/scripts/run-tests.sh $(SRC_DIR)

# DOCKER

dev-env-up:
	sudo chmod 666 /var/run/docker.sock
	docker volume create --name=db-pg-data
	docker-compose -p $(PROJECT) -f docker/docker-compose.yaml up -d --build --remove-orphans
	@while [ $$(docker ps --filter health=starting --format "{{.Status}}" | wc -l) != 0 ]; do echo 'waiting for healthy containers'; sleep 1; done

dev-env-down:
	docker-compose -p $(PROJECT) -f docker/docker-compose.yaml down -v --remove-orphans

dev-env-clean: dev-env-down
	docker volume remove db-pg-data
	echo "Clean"

# UTILS

run-static-analysis:
	.cicd/scripts/run_static_analysis.sh forum/forum/