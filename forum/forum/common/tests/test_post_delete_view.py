from django.test import Client, TestCase
from django.urls import reverse_lazy

from forum.common.test_mixins import TestMixin
from forum.common.models import Category, Topic, Post


class TestPostDeleteView(TestMixin, TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.client = Client()
        self.category, _ = Category.objects.get_or_create({
            'name': 'Test category',
            'order_number': 1,
        })
        self.topic, _ = Topic.objects.get_or_create({
            'name': 'This is test topic',
            'category_id': self.category.pk,
        })

    def get_post(self, **kwargs):
        kwargs_def = {
            'topic_id': self.topic.pk
        }
        kwargs_def.update(kwargs)
        return Post.objects.create(**kwargs_def)

    def test_post_block_view_load_superuser(self):
        user = self.get_or_create_superuser()
        self.client.force_login(user)

        topic = self.topic
        response = self.client.get(reverse_lazy('common:block_topic', kwargs={'pk': topic.pk}))

        self.assertContains(response, 'Are you sure you want block topic', status_code=200)
        self.assertContains(response, user.username)

    def test_post_block_view_load_user(self):
        user = self.get_or_create_user()
        self.client.force_login(user)

        topic = self.topic
        response = self.client.get(reverse_lazy('common:block_topic', kwargs={'pk': topic.pk}))

        self.assertEqual(403, response.status_code)

    def test_post_block_view_user_does_not_see_block_button(self):
        user = self.get_or_create_user()
        self.client.force_login(user)

        category = self.category

        response = self.client.get(reverse_lazy('common:topics', kwargs={'pk': category.pk}))
        self.assertNotContains(response, 'Block', status_code=200)

    def test_post_block_view_superuser_does_see_block_button(self):
        user = self.get_or_create_superuser()
        self.client.force_login(user)

        category = self.category

        response = self.client.get(reverse_lazy('common:topics', kwargs={'pk': category.pk}))
        self.assertContains(response, 'Block', status_code=200)

    def test_post_delete_view_not_load_for_unauthenticated_user(self):
        user = self.get_or_create_user()
        post = self.get_post(author_id=user.pk, text=self.faker.text())
        response = self.client.get(reverse_lazy('common:delete_post', kwargs={'pk': post.pk}))

        self.assertEqual(302, response.status_code)
        self.assertTrue('/login' in response.url)

    def test_post_delete_view_load_for_author(self):
        user = self.get_or_create_user()
        self.client.force_login(user)

        text = self.faker.text()
        post = self.get_post(author_id=user.pk, text=text)
        response = self.client.get(reverse_lazy('common:delete_post', kwargs={'pk': post.pk}))

        self.assertContains(response, 'Are you sure you want delete post?', status_code=200)
        self.assertContains(response, user.username)
        self.assertContains(response, text)

    def test_post_delete_view_load_for_superauthor(self):
        user = self.get_or_create_superuser()
        self.client.force_login(user)

        text = self.faker.text()
        post = self.get_post(author_id=user.pk, text=text)
        response = self.client.get(reverse_lazy('common:delete_post', kwargs={'pk': post.pk}))

        self.assertContains(response, 'Are you sure you want delete post?', status_code=200)
        self.assertContains(response, user.username)
        self.assertContains(response, text)

    # def test_post_delete_view_load_for_another_user(self):
    #     user = self.get_or_create_user()
    #     user2 = self.get_or_create_user()
    #     self.client.force_login(user)
    #
    #     text = self.faker.text()
    #     post = self.get_post(author_id=user2.pk, text=text)
    #     response = self.client.get(reverse_lazy('common:delete_post', kwargs={'pk': post.pk}))
    #
    #     self.assertEqual(302, response.status_code)
    #     self.assertTrue(
    #         response.url,
    #         reverse_lazy('common:topic_posts', kwargs={'pk': post.topic_id}),
    #     )
    #     messages = list(get_messages(response.wsgi_request))
    #     self.assertEqual(len(messages), 1)
    #     self.assertEqual(str(messages[0]), 'Removing post is not allowed!')
